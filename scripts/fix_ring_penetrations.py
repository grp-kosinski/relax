#!/usr/bin/python

"""
Run like this:
ChimeraX --nogui --exit --script "../scripts/optimize_rotamers.py data/hNup96_CRouter_processed.pdb data/hNup96_CRouter_processed.top hNup96_CRouter_fixed.pdb"
"""

import os
import subprocess
import argparse
import importlib.util
from chimerax.core.commands import run
relax_dir_path = os.path.dirname(os.path.dirname(__file__))
spec = importlib.util.spec_from_file_location("module.name", os.path.join(relax_dir_path, 'scripts', 'pentest.py'))
pentest = importlib.util.module_from_spec(spec)
spec.loader.exec_module(pentest)

parser = argparse.ArgumentParser(description='Fix side chains penetrating rings by rotamer optimization using ChimeraX')
parser.add_argument('pdbfilename', type=str, help='Input PDB filename')
parser.add_argument('topfilename', type=str, help='Input TOP filename')
parser.add_argument('out_pdbfilename', type=str, help='Output PDB filename')
args = parser.parse_args()

run(session, 'open '+args.pdbfilename)

top = pentest.build_topology(args.topfilename, args.pdbfilename)
crd = pentest.build_atomtable(top, args.pdbfilename)
pairs, rings = pentest.check_ring_penetration(top, crd)
for out in pentest.get_ring_penetration_formatted(pairs, rings, top):
    print(
            out['penetrating_pair_chainid'],
            out['penetrating_pair_resid'],
            out['penetrating_pair_resname']
        )
    run(session, 'swapaa /{penetrating_pair_chainid}:{penetrating_pair_resid} {penetrating_pair_resname}'.format(**out))
run(session, 'save '+ args.out_pdbfilename +' models #1')
