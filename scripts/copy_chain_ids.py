#!/usr/bin/python

'''
Copy numbering from one PDB file to another.
'''
import sys
import os
from itertools import groupby

def is_chainy(line):
    '''
    Return True if the passed PDB line contains chain id according to PDB format spec
    '''
    if line[0:6] in ('ATOM  ', 'HETATM', 'ANISOU'):
        return True
    if line[0:6] == 'TER   ':
        if len(line) > 21 and line[21] != ' ':
            return True

    return False

def chainy_lines(f):
    for line in f:
        if is_chainy(line):
            yield True, line
        else:
            yield False, line

def resi_grouper(sth):
    is_coord, line = sth
    if is_coord:
        chain = line[21]
        resi = line[22:26]
        marker = chain + resi
    else:
        marker = line
    
    return marker

def copy_chain(line, new_chain):
    new_line = ''.join([line[:21], new_chain, line[22:]])

    if new_line[-1] != '\n':
        new_line = new_line + '\n'
    # print new_line
    return new_line

def copy_chain_ids(f1, f2):
    chains = []
    # Group by resi to cover unequal number of atoms (e.g. with and without hydrogens)
    for k, lines in groupby(chainy_lines(f1), resi_grouper):
        is_coord, line = next(lines)

        if is_coord:
            chains.append(line[21])
    # print(chains)
    resi_count = 0
    out_lines = []
    for k, lines in groupby(chainy_lines(f2), resi_grouper):
        is_coord, line = next(lines)

        if is_coord:

            out_lines.append(copy_chain(line, chains[resi_count]))
            for is_coord, line in lines:
                out_lines.append(copy_chain(line, chains[resi_count]))

            resi_count = resi_count + 1
        else:
            # print line
            out_lines.append(line)
            for is_coord, line in lines:
                out_lines.append(line)           

    sys.stdout.write(''.join(out_lines)) #using sys.stdout.write because print would add extra end of the file

def main():
    if len(sys.argv) < 3 or sys.argv[1] in ('-h', '--help'):
        usage = '''Usage: {0} pdb_file_from pdb_filename_to > out.pdb

Copy chain ids from one to another  with no other changes to the PDB file.

Doesn't uses any external parsers, just locates and changes the chains, and nothing else.

        '''.format(os.path.basename(sys.argv[0]))
        print(usage)
        exit()
    pdb_filename1 = sys.argv[1]
    pdb_filename2 = sys.argv[2]

    with open(pdb_filename1) as f1:
        with open(pdb_filename2) as f2:
            copy_chain_ids(f1, f2)

if __name__ == '__main__':
    main()