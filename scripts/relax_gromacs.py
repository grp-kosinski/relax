#!/usr/bin/python
import subprocess
import sys
import os
import re
import glob
import tempfile
import shutil
import argparse

parser = argparse.ArgumentParser(description='Minimize a protein with GROMACS')
parser.add_argument('in_filename', type=str, help='Input PDB filename')
parser.add_argument('out_filename', type=str, help='Output PDB filename')
parser.add_argument('--mode', type=str, default='cpu', choices=['cpu', 'gpu'], help='Run on CPU or GPU')
parser.add_argument('--gpu_count', type=int, default=1, help='Number of GPUs to use')
parser.add_argument('--cpu_count', type=int, default=1, help='Number of CPUs to use')
parser.add_argument('--solvate', default=False, action='store_true', help='Minimize in explicit solvent')

paramsdir = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'relax/params')
if not os.path.exists(paramsdir):
    try:
        import relax
        paramsdir = os.path.join(os.path.dirname(os.path.abspath(relax.__file__)), 'params')
    except ImportError:
        pass
if not os.path.exists(paramsdir):
    print("Can't find params")
    sys.exit()

parser.add_argument('--ions_file', type=str, default=f'{paramsdir}/gromacs/ions.mdp', help='Custom ions.mdp file')
parser.add_argument('--minim_file', type=str, default=f'{paramsdir}/gromacs/minim.mdp', help='Custom minim.mdp file')
parser.add_argument('--keep_files_dirname', type=str, default=None, help='Directory name to keep the intermediate files')
parser.add_argument('--fix_ring_penetrations', default=False, action='store_true', help='Fix sidechains penetrating rings by optimizing rotamers. Requires ChimeraX.')

args = parser.parse_args()

prefix = os.path.splitext(os.path.basename(args.in_filename))[0]

OMP_NUM_THREADS = os.environ.get('OMP_NUM_THREADS') or 1
charge = 0

def run_cmd(cmd):
    print(cmd)
    try:
        output = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT, cwd=tempdirpath)
    except subprocess.CalledProcessError as exc:
        print("Status : FAIL", exc.returncode, exc.output.decode('utf-8'))
        sys.exit()
    else:
        print(output.decode('utf-8'))
    return output

if not args.keep_files_dirname:
    tempdirpath = tempfile.mkdtemp()
else:
    tempdirpath = args.keep_files_dirname
    os.makedirs(tempdirpath, exist_ok=True)

args.out_filename = os.path.abspath(args.out_filename)
scriptdirpath = os.path.dirname(os.path.abspath(__file__))
try:
    shutil.copy2(args.in_filename, tempdirpath)


    shutil.copy2(args.ions_file, tempdirpath)
    shutil.copy2(args.minim_file, tempdirpath)

    cmds = [
            f'cat {os.path.basename(args.in_filename)} | grep -v " OXT " | egrep "^ATOM" > {prefix}_clean.pdb',
            f'python {scriptdirpath}/insert_TERs.py {prefix}_clean.pdb > {prefix}_TERs.pdb',
            f'gmx pdb2gmx -f {prefix}_TERs.pdb -o {prefix}_processed.pdb -water spce -merge all -ignh <<< 6',
            ]
    if args.fix_ring_penetrations:
        cmds.extend([
            f'python {scriptdirpath}/pentest.py topol.top {prefix}_processed.pdb',
            f'ChimeraX --nogui --exit --script "{scriptdirpath}/fix_ring_penetrations.py {prefix}_processed.pdb topol.top {prefix}_processed_fixpenet.pdb"',
            f'python {scriptdirpath}/insert_TERs.py {prefix}_processed_fixpenet.pdb > {prefix}_processed_fixpenet_TERs.pdb',
            f'gmx pdb2gmx -f {prefix}_processed_fixpenet_TERs.pdb -o {prefix}_processed_fixpenet1.pdb -water spce -merge all -ignh <<< 6',
            ])
        processed_file = f'{prefix}_processed_fixpenet1.pdb'
    else:
        processed_file = f'{prefix}_processed.pdb'

    cmds.extend([
            #the following moves the molecule, perhaps we could avoid it with -noc and setting the box differently?
            f'gmx editconf -f {processed_file} -o {prefix}_newbox.pdb -c -d 7.0 -bt cubic', #big box for elongated structures
            ])
    if args.solvate:
        cmds.extend([
            f'gmx solvate -cp {prefix}_newbox.pdb -cs spc216.gro -o {prefix}_solv.pdb -p topol.top',
            f'gmx grompp -f ions.mdp -c {prefix}_solv.pdb -p topol.top -o ions.tpr -maxwarn 1',
            f'printf "SOL" | gmx genion -s ions.tpr -o {prefix}_solv_ions.pdb -p topol.top -pname NA -nname CL --neutral <<< 13',
            f'gmx grompp -f minim.mdp -c {prefix}_solv_ions.pdb -p topol.top -o em.tpr -maxwarn 1',
            ])
    else:
        cmds.extend([
                f'gmx grompp -f minim.mdp -c {prefix}_newbox.pdb -p topol.top -o em.tpr -maxwarn 1',
                ])
    if args.mode == 'cpu':
        cmds.append(
            f'gmx mdrun -nt {args.cpu_count} -v -deffnm em -c {prefix}.min.temp.pdb'
            )
    elif args.mode == 'gpu':
        cmds.append(
            f'gmx mdrun -deffnm em -ntmpi {args.gpu_count} -nb gpu -ntomp {OMP_NUM_THREADS} -c {prefix}.min.temp.pdb',
        )

    shift = None
    for cmd in cmds:
        output = run_cmd(cmd)

        #Store the shift for moving the molecule back
        if cmd.startswith('gmx editconf') and not shift:
            m = re.search(r'shift       :(.*?)\(nm\)', str(output))
            if m:
                x = -float(m.groups()[0][:7])
                y = -float(m.groups()[0][7:14])
                z = -float(m.groups()[0][14:22])
                shift = ' '.join(f'{o:.3f}' for o in (x,y,z))
            else:
                raise Exception("Can't determine shift")

    cmds = [
            f'gmx trjconv -f {prefix}.min.temp.pdb -s em.tpr -nocenter -o {prefix}.min.temp1.pdb <<< 2',
            f'gmx editconf -f {prefix}.min.temp1.pdb -translate {shift} -o {prefix}.min.temp1a.pdb',
            f'python {scriptdirpath}/gromacs2pdb.py {prefix}.min.temp1a.pdb > {prefix}.min.temp2.pdb',
            f'python {scriptdirpath}/copy_chain_ids.py {prefix}_TERs.pdb {prefix}.min.temp2.pdb > {args.out_filename}',
            ]
    

    for cmd in cmds:
        output = run_cmd(cmd)

finally:
    if not args.keep_files_dirname:
        shutil.rmtree(tempdirpath)
    else:
        print('Directory with intermediate files:', tempdirpath)
