"""
module load ccp4
ccp4-python ../scripts/validate.py data/hAladin_IRcyt_outer.pdb

#Ask Thomas to install this: https://github.com/cctbx/cctbx_project
"""
import sys

from iotbx.data_manager import DataManager    # Load in the DataManager
dm = DataManager()                            # Initialize the DataManager and call it dm
dm.set_overwrite(True)       # tell the DataManager to overwrite files with the same name

model_filename=sys.argv[1]                         #   Name of model file
dm.process_model_file(model_filename)              #   Read in data from model file
model = dm.get_model(model_filename)               #   Deliver model object with model info

from iotbx.pdb import hierarchy
from mmtbx.validation import molprobity
pdb_in = hierarchy.input(file_name=sys.argv[1])

hierarchy = pdb_in.hierarchy
validation = molprobity.molprobity(model, outliers_only=False)

methods = [
    'cbeta_outliers',
    'clashscore',
    'rama_allowed',
    'rama_favored',
    'rama_outliers',
    'rms_angles',
    'rms_bonds',
    'rota_outliers',
]

for method in methods:
    print(method, getattr(validation, method)())

validation.show(show_percentiles=True)