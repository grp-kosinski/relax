import os
import unittest
import shutil
import tempfile
import subprocess
import glob
from itertools import groupby
import numpy

MODE = 'gpu'
FAST = True

def is_tool(name):
    """Check whether `name` is on PATH and marked as executable."""

    # from whichcraft import which
    from shutil import which

    if which(name) is not None:
        return name
    else:
        return False

def is_coord_line(line):
    return line[0:6] in ('ATOM  ')

def coord_lines(f):
    for line in f:
        if is_coord_line(line):
            yield line

def get_resi_coords(f, chain_id, resid):
    for line in coord_lines(f):
        if chain_id == line[21] and resid == int(line[22:26]) and line[13:15] == 'CA':
            return float(line[30:38]), float(line[38:46]), float(line[46:54])

def get_all_coords(f):
    for line in coord_lines(f):
        yield float(line[30:38]), float(line[38:46]), float(line[46:54])

def get_CA_coords(f):
    for line in coord_lines(f):
        if line[13:15] == 'CA':
            yield float(line[30:38]), float(line[38:46]), float(line[46:54])

def get_resi_count(f):
    return len([line for line in coord_lines(f) if line[13:15] == 'CA'])


def resi_grouper(line):
    chain = line[21]
    resi = line[22:26]
    marker = chain + resi
    
    return marker

def chain_grouper(line):
    
    return line[21]

def ranges_from_list(i):
    # https://stackoverflow.com/questions/4628333/converting-a-list-of-integers-into-range-in-python
    for a, b in groupby(enumerate(i), lambda pair: pair[1] - pair[0]):
        b = list(b)
        yield b[0][1], b[-1][1]

def get_coverage(f):
    cov = []
    for chain_id, chain_lines in groupby(coord_lines(f), chain_grouper):
        resi_nums = []
        for k, lines in groupby(chain_lines, resi_grouper):
            line = next(lines)
            resi_nums.append(int(line[22:26]))

        for first, second in ranges_from_list(resi_nums):
            cov.append([chain_id, first, second])

    return cov

def calc_rmsd(V, W):
    """
    Calculate Root-mean-square deviation from two sets of vectors V and W.
    Parameters
    ----------
    V : array
        (N,D) matrix, where N is points and D is dimension.
    W : array
        (N,D) matrix, where N is points and D is dimension.
    Returns
    -------
    rmsd : float
        Root-mean-square deviation between the two vectors
    """
    diff = numpy.array(V) - numpy.array(W)
    N = len(V)
    return numpy.sqrt((diff * diff).sum() / N)


relax_dir_path = os.path.dirname(os.path.dirname(__file__))

class _TestBase(object):
    def _do_tests(self):
        with open(self.in_filename) as in_file:
            with open(self.out_filename) as out_file:
                in_file_lines = [line for line in in_file.read().splitlines() if line.strip()]
                out_file_lines = [line for line in out_file.read().splitlines() if line.strip()]
                self.assertEqual(get_resi_count(in_file_lines), get_resi_count(out_file_lines))
                print(get_coverage(in_file_lines), get_coverage(out_file_lines))
                self.assertEqual(get_coverage(in_file_lines), get_coverage(out_file_lines))
                rmsd = calc_rmsd(list(get_CA_coords(in_file_lines)), list(get_CA_coords(out_file_lines)))
                self.assertLess(rmsd, 1)

    def test_Monomer(self):
        """One chain, no missing residues, 2 mins on A100
        """
        self.in_filename = os.path.abspath('data/unrelaxed_AlphaFold_model.pdb')
        self.testdir = tempfile.mkdtemp()
        self.out_filename = os.path.join(self.testdir, os.path.basename(self.in_filename.replace('.pdb','.min.pdb')))        
        self._run_relax()
        self._do_tests()

    def test_5cqs(self):
        """Two chains, missing residues, 5 mins on A100
        """
        self.in_filename = os.path.abspath('data/5cqs.dimer.complete.pdb')
        self.testdir = tempfile.mkdtemp()
        self.out_filename = os.path.join(self.testdir, os.path.basename(self.in_filename.replace('.pdb','.min.pdb')))
        self._run_relax()
        self._do_tests()

    def test_hNup53(self):
        """Two chains, was making problems with OXT and shift detection
        """
        self.in_filename = os.path.abspath('data/hNup53_IRnuc_inner.pdb')
        self.testdir = tempfile.mkdtemp()
        self.out_filename = os.path.join(self.testdir, os.path.basename(self.in_filename.replace('.pdb','.min.pdb')))
        self._run_relax()
        self._do_tests()

    def test_Aladin(self):
        """One chain, negative value for shift
        """
        self.in_filename = os.path.abspath('data/hAladin_IRcyt_outer.pdb')
        self.testdir = tempfile.mkdtemp()
        self.out_filename = os.path.join(self.testdir, os.path.basename(self.in_filename.replace('.pdb','.min.pdb')))
        self._run_relax()
        self._do_tests()

    def test_Nup133(self):
        """One chain, negative value for shift, no spaces between shift values
        """
        self.in_filename = os.path.abspath('data/hNup133_CRinner.pdb')
        self.testdir = tempfile.mkdtemp()
        self.out_filename = os.path.join(self.testdir, os.path.basename(self.in_filename.replace('.pdb','.min.pdb')))
        self._run_relax()
        self._do_tests()

    def test_Nup93(self):
        """One chain, was doing some crazy shit
        """
        self.in_filename = os.path.abspath('data/hNup93_CRouter.pdb')
        self.testdir = tempfile.mkdtemp()
        self.out_filename = os.path.join(self.testdir, os.path.basename(self.in_filename.replace('.pdb','.min.pdb')))
        self._run_relax()
        self._do_tests()

    def test_hNup96_CRinner_CRouter(self):
        """Two chains with identical chain ID, has ring penetration
        """
        self.in_filename = os.path.abspath('data/hNup96_CRinner_CRouter.pdb')
        self.testdir = tempfile.mkdtemp()
        self.out_filename = os.path.join(self.testdir, os.path.basename(self.in_filename.replace('.pdb','.min.pdb')))
        self._run_relax()
        self._do_tests()



class TestGromacsVacuum(_TestBase, unittest.TestCase):
    script_path = is_tool('relax_gromacs.py') \
        or f'python {relax_dir_path}/scripts/relax_gromacs.py'

    def _run_relax(self):
        cmd = f'{self.script_path} --mode {MODE} --gpu_count 1 {self.in_filename} {self.out_filename}'
        if FAST:
            cmd += f" --minim_file {os.path.abspath('data/minim.mdp')}"
        print(cmd)
        subprocess.call(cmd, shell=True, cwd=self.testdir)

    def tearDown(self):
        #print(self.out_filename)
        shutil.rmtree(self.testdir)


class TestGromacsSolvent(_TestBase, unittest.TestCase):
    script_path = is_tool('relax_gromacs.py') \
        or f'python {relax_dir_path}/scripts/relax_gromacs.py'

    def _run_relax(self):
        cmd = f'{self.script_path} --mode {MODE} --gpu_count 1 --solvate {self.in_filename} {self.out_filename}'
        if FAST:
            cmd += f" --minim_file {os.path.abspath('data/minim.mdp')}"
        print(cmd)
        subprocess.call(cmd, shell=True, cwd=self.testdir)

    def tearDown(self):
        # print(self.out_filename)
        shutil.rmtree(self.testdir)

class TestGromacs_fix_ring_penetrations(_TestBase, unittest.TestCase):
    script_path = is_tool('relax_gromacs.py') \
        or f'python {relax_dir_path}/scripts/relax_gromacs.py'

    def _run_relax(self):
        cmd = f'{self.script_path} --mode {MODE} --gpu_count 1 --fix_ring_penetrations {self.in_filename} {self.out_filename}'
        if FAST:
            cmd += f" --minim_file {os.path.abspath('data/minim.mdp')}"
        print(cmd)
        subprocess.call(cmd, shell=True, cwd=self.testdir)

    def tearDown(self):
        #print(self.out_filename)
        shutil.rmtree(self.testdir)

if __name__ == '__main__':
    unittest.main()