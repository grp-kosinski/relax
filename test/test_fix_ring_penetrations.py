import unittest
import os
import subprocess
import tempfile
import shutil
import importlib.util
relax_dir_path = os.path.dirname(os.path.dirname(__file__))
spec = importlib.util.spec_from_file_location("module.name", os.path.join(relax_dir_path, 'scripts', 'pentest.py'))
pentest = importlib.util.module_from_spec(spec)
spec.loader.exec_module(pentest)

def is_tool(name):
    """Check whether `name` is on PATH and marked as executable."""

    # from whichcraft import which
    from shutil import which

    if which(name) is not None:
        return name
    else:
        return False

class _TestBase(object):
    def test_run_script(self):
        top = pentest.build_topology(self.topfile, self.pdbfile)
        crd = pentest.build_atomtable(top, self.pdbfile)
        pairs, rings = pentest.check_ring_penetration(top, crd)
        self.assertListEqual(self.penetrating_pairs, pairs)
        self.assertListEqual(self.penetrated_rings, rings)

        for expected_out, out in zip(self.penetration_formatted, pentest.get_ring_penetration_formatted(pairs, rings, top)):
            self.assertDictEqual(expected_out, out)

        self.testdir = tempfile.mkdtemp()
        tempfilename = os.path.join(self.testdir, 'fixed.pdb')

        script_path = is_tool('fix_ring_penetrations.py') \
            or f'{relax_dir_path}/scripts/fix_ring_penetrations.py'

        cmd = f'ChimeraX --nogui --exit --script "{script_path} {self.pdbfile} {self.topfile} {tempfilename}"'
        print(cmd)
        subprocess.run(cmd, shell=True)
        scriptdirpath = os.path.join(relax_dir_path, 'scripts')
        cmds = [
            f'python {scriptdirpath}/insert_TERs.py fixed.pdb > fixed_TERs.pdb',
            f'gmx pdb2gmx -f fixed_TERs.pdb -o fixed1.pdb -water spce -merge all -ignh <<< 6',
        ]
        for cmd in cmds:
            subprocess.run(cmd, shell=True, cwd=self.testdir)

        top = pentest.build_topology(self.topfile, os.path.join(self.testdir, 'fixed1.pdb'))
        crd = pentest.build_atomtable(top, os.path.join(self.testdir, 'fixed1.pdb'))
        pairs, rings = pentest.check_ring_penetration(top, crd)

        self.assertEqual(0, len(pairs))
        self.assertEqual(0, len(rings))

    def tearDown(self):
        shutil.rmtree(self.testdir)

class Test_hNup96_CRouter(_TestBase, unittest.TestCase):
    """Two chains with identical chain ID, has ring penetration
    """
    topfile = os.path.abspath('data/hNup96_CRouter_processed.top')
    pdbfile = os.path.abspath('data/hNup96_CRouter_processed.pdb')
    penetrating_pairs = [(8068, 8071)]
    penetrated_rings = [[8101, 8103, 8105, 8114, 8100]]
    penetration_formatted = [{'penetrating_pair_chainid': 'M', 'penetrating_pair_resid': '761', 'penetrating_pair_resname': 'GLU', 'penetrating_pair_atnames': ['CG', 'CD'], 'penetrated_ring_chainid': 'M', 'penetrated_ring_resid': '763', 'penetrated_ring_resname': 'TRP', 'penetrated_ring_atnames': ['CD1', 'NE1', 'CE2', 'CD2', 'CG']}]

if __name__ == '__main__':
    unittest.main()