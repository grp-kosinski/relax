from distutils.core import setup

setup(
    name='relax',
    version='0.0.1dev',
    author='Jan Kosinski',
    author_email='jan.kosinski@embl.de',
    scripts=[
        'scripts/relax_gromacs.py',
        'scripts/gromacs2pdb.py',
        'scripts/copy_chain_ids.py',

             ],
    packages=['relax'],
    include_package_data = True,
    package_data={'relax': [
                        'params/gromacs/ions.mdp', 
                        'params/gromacs/minim.mdp'
                    ]
    },
    url='none',
    license='LICENSE.txt',
    description='Scripts to minimize macromolecular structures, for example unrelaxed AlphaFold models.',
)

