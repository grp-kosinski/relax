Scripts to minimize macromolecular structures, for example unrelaxed AlphaFold models.
Producing nice PDB files, and keeping original chain IDs.
Keeping the PDB in the original reference frame.
Optionally resolving side chains threading rings of aromatic side chains.

Example usage
=============

When using --fix_ring_penetrations option, you need to install ChimeraX and have ChimeraX command in your path. On the EMBL cluster, load ChimeraX like hits:

    module load ChimeraX

Then install Python3 or load as below:

    module load Python/3.9.6-GCCcore-11.2.0

At last, install (or load) GROMACS, for CPU:

    module load GROMACS

and for GPU:

    module load GROMACS/2021.4-foss-2021b-CUDA-11.5.1

Note, you probably must load the above modules in the above order! Otherwise there are some Python version conflicts with ChimeraX.

Run:

    python scripts/relax_gromacs.py \
        --mode gpu \
        --gpu_count 1 \
        --fix_ring_penetrations \
        test/data/unrelaxed_AlphaFold_model.pdb test/data/unrelaxed_AlphaFold_model.min.pdb

More options
============
    
    usage: relax_gromacs.py [options] in_filename out_filename

    Minimize a protein with GROMACS

    positional arguments:
    in_filename           Input PDB filename
    out_filename          Output PDB filename

    optional arguments:
    -h, --help            show this help message and exit
    --mode {cpu,gpu}      Run on CPU or GPU
    --gpu_count GPU_COUNT
                            Number of GPUs to use
    --cpu_count CPU_COUNT
                            Number of GPUs to use
    --solvate             Minimize in explicit solvent
    --ions_file IONS_FILE
                            Custom ions.mdp file
    --minim_file MINIM_FILE
                            Custom minim.mdp file
    --keep_files_dirname KEEP_FILES_DIRNAME
                            Directory name to keep the intermediate files
    --fix_ring_penetrations
                            Fix sidechains penetrating rings by optimizing rotamers. Requires ChimeraX.

Developing
==========

Have you modifed `relax_gromacs.py` script and want to contribute the edits to this repo? Run `test_relax.py` in `test` directory before commiting!

Want to add a new relax script? Add tests by simply adding a new class to `test_relax.py` that looks like:

```python
class TestGromacs(_TestBase, unittest.TestCase):
    script_path = is_tool('relax_gromacs.py') \
        or f'python {relax_dir_path}/scripts/relax_gromacs.py'

    def _run_relax(self):
        cmd = f'{self.script_path} --mode {MODE} --gpu_count 1 {self.in_filename} {self.out_filename}'
        if FAST:
            cmd += f" --minim_file {os.path.abspath('data/minim.mdp')}"
        print(cmd)
        subprocess.call(cmd, shell=True, cwd=self.testdir)

    def tearDown(self):
        shutil.rmtree(self.testdir)
```

It needs to contain your modified `_run_relax` method that runs your script and saves relaxed file to `self.out_filename`.
Also, keep the `tearDown` method to remove the temporary directory.
